import java.util.Scanner;
import java.util.ArrayList;

/**
 * Chess Simulator
 * @author Justin Valeroso
 * @author Travis Thiel
 *
 */

public class Chess {
	// Keep track of previous input for en passant, resign, draw
	static String prevInput = "";
	static int prevSelectFile = -1;
	static int prevSelectRank = -1;
	static int prevDestFile = -1;
	static int prevDestRank = -1;
	
	// Set flags for special moves to be handled by processMove
	static boolean castleFlag = false;
	static boolean enPassantFlag = false;
	
	// Keep track of eligibility for castle
	static boolean whiteCastleRook1 = true;
	static boolean whiteCastleRook2 = true;
	static boolean whiteCastleKing = true;
	static boolean blackCastleRook1 = true;
	static boolean blackCastleRook2 = true;
	static boolean blackCastleKing = true;
	
	// Keep track of player in check
	static boolean inCheck = false;
	
	public static void main (String[] args) {
		
		// Initialize the chess board, scanner, and turn
		String [][] chessBoard = {{"bR", "bN", "bB", "bQ", "bK", "bB", "bN", "bR", "8 "},
								  {"bp", "bp", "bp", "bp", "bp", "bp", "bp", "bp", "7 "},
								  {"  ", "##", "  ", "##", "  ", "##", "  ", "##", "6 "},
								  {"##", "  ", "##", "  ", "##", "  ", "##", "  ", "5 "},
								  {"  ", "##", "  ", "##", "  ", "##", "  ", "##", "4 "},
								  {"##", "  ", "##", "  ", "##", "  ", "##", "  ", "3 "},
								  {"wp", "wp", "wp", "wp", "wp", "wp", "wp", "wp", "2 "},
								  {"wR", "wN", "wB", "wQ", "wK", "wB", "wN", "wR", "1 "},
								  {"a ", "b ", "c ", "d ", "e ", "f ", "g ", "h ", "  "}};
		
		Scanner sc = new Scanner(System.in);
		boolean whiteTurn = true;		
		
		// Main game loop
		while (true) {
			printChessBoard(chessBoard);
			if (whiteTurn) {
				System.out.println("White's turn: " );
			}
			else {
				System.out.println("Black's turn: ");
			}
			
			// Ask user for move, and if illegal, keep prompting user
			String input = sc.nextLine();
			while (!isValidMove(chessBoard, input, whiteTurn)) {
				System.out.println("Illegal move, try again \n");
				if (whiteTurn) {
					System.out.println("White's turn: " );
				}
				else {
					System.out.println("Black's turn: ");
				}
				input = sc.nextLine();
			}
			
			// Process the move 
			processMove(chessBoard, input, whiteTurn);
			
			// Check to see if King is in check
			if (isCheck(chessBoard, whiteTurn)) {
				if (isCheckmate(chessBoard, whiteTurn)) {
					System.out.println("Checkmate");
					if (whiteTurn) {
						System.out.println("White wins");
						System.exit(0);
					}
					else {
						System.out.println("Black wins");
						System.exit(0);
					}
				}
				else {
					System.out.println("Check");
				}
				inCheck = true;
			}/*else if(isStalemate(chessBoard)) {
				System.out.println("Stale Mate");
				System.exit(0);
			}*/
			
			
			// Switch turns
			whiteTurn = !whiteTurn; 
			
			// Keep track of previous input
			prevInput = input;
		}
	}
	
	/**
	 * Checks player input to see if the move is either a valid draw, resign, or a piece move 
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param input the players input
	 * @param whiteTurn the boolean value which is true if it is the white player's turn or false if it is the black player's turn 
	 * @return true if the move is valid, false if not
	 */
	
	public static boolean isValidMove(String [][] chessBoard, String input, boolean whiteTurn) {
		// Check to see if input is resign or draw
		if (input.toLowerCase().equals("resign")) {
			return true;
		}
		else if (input.toLowerCase().equals("draw")) {
			// If input is "draw", check to see if other player asked for draw
			if (prevInput.toLowerCase().contains("draw?")) {
				return true; 	
			}
			else {
				return false;
			}
		}
		
		// Parse input into selectFile, selectRank, destFile, destRank
		int selectFile, selectRank, destFile, destRank;
		
		// Try to parse the input of user, and catch exceptions when parsing
		// the raw String data
		char rawSelectFile, rawDestFile;
		int rawSelectRank, rawDestRank;
		try {
			rawSelectFile = Character.toLowerCase(input.charAt(0));
			rawSelectRank = Integer.parseInt(input.substring(1,2));
			rawDestFile = Character.toLowerCase(input.charAt(3));
			rawDestRank = Integer.parseInt(input.substring(4,5));
		} 
		catch (NumberFormatException e) {
			return false;
		}
		catch (IndexOutOfBoundsException e) {
			return false;
		}
		
		
		// Use switch-case statements to turn raw values into values that
		// the program can use
		switch(rawSelectFile) {
			case 'a': 
				selectFile = 0;
				break;
			case 'b':
				selectFile = 1;
				break;
			case 'c':
				selectFile = 2;
				break;
			case 'd':
				selectFile = 3;
				break;
			case 'e':
				selectFile = 4;
				break;
			case 'f':
				selectFile = 5;
				break;
			case 'g':
				selectFile = 6;
				break;
			case 'h':
				selectFile = 7;
				break;
			default:
				return false;
		}
		switch(rawSelectRank) {
			case 8:
				selectRank = 0;
				break;
			case 7:
				selectRank = 1;
				break;
			case 6:
				selectRank = 2;
				break;
			case 5: 
				selectRank = 3;
				break;
			case 4:
				selectRank = 4;
				break;
			case 3:
				selectRank = 5;
				break;
			case 2:
				selectRank = 6;
				break;
			case 1:
				selectRank = 7;
				break;
			default:
				return false;
		}	
		switch(rawDestFile) {
			case 'a': 
				destFile = 0;
				break;
			case 'b':
				destFile = 1;
				break;
			case 'c':
				destFile = 2;
				break;
			case 'd':
				destFile = 3;
				break;
			case 'e':
				destFile = 4;
				break;
			case 'f':
				destFile = 5;
				break;
			case 'g':
				destFile = 6;
				break;
			case 'h':
				destFile = 7;
				break;
			default:
				return false;
		}		
		switch(rawDestRank) {
			case 8:
				destRank = 0;
				break;
			case 7:
				destRank = 1;
				break;
			case 6:
				destRank = 2;
				break;
			case 5: 
				destRank = 3;
				break;
			case 4:
				destRank = 4;
				break;
			case 3:
				destRank = 5;
				break;
			case 2:
				destRank = 6;
				break;
			case 1:
				destRank = 7;
				break;
			default:
				return false;
		}
			
		// Check to see if the piece selected is their's
		if (whiteTurn) {
			if (chessBoard[selectRank][selectFile].charAt(0) != 'w') {
				return false;
			}
		}
		else {
			if (chessBoard[selectRank][selectFile].charAt(0) != 'b') {
				return false;
			}
		}
		
		// If in check, check to see if moving the piece to the desired location (regardless of if valid or not)
		// would still leave that player in check. If so, returns false
		if (inCheck) {
			String [][] proxyChessBoard = new String[chessBoard.length][chessBoard.length];
			copyBoard(proxyChessBoard, chessBoard);
			String temp = proxyChessBoard[selectRank][selectFile];
			proxyChessBoard[selectRank][selectFile] = "  ";
			proxyChessBoard[destRank][destFile] = temp;
			
			// Use !whiteTurn because you're checking to see if the current player who's turn it is is in check
			// and not their opponent
			if(isCheck(proxyChessBoard, !whiteTurn)) {
				return false;
			}
		}
		
		// Another switch-case to redirect and help mitigate 
		// confusion with isValidMove logic
		switch (chessBoard[selectRank][selectFile].charAt(1)) {
			case 'p':
				return isValidMovePawn(chessBoard, selectRank, selectFile, destRank, destFile, whiteTurn);
			case 'R':
				return isValidMoveRook(chessBoard, selectRank, selectFile, destRank, destFile);
			case 'N':
				return isValidMoveKnight(chessBoard, selectRank, selectFile, destRank, destFile);
			case 'B':
				return isValidMoveBishop(chessBoard, selectRank, selectFile, destRank, destFile);
			case 'Q':
				return isValidMoveQueen(chessBoard, selectRank, selectFile, destRank, destFile);
			case 'K':
				return isValidMoveKing(chessBoard, selectRank, selectFile, destRank, destFile);
			default:
				System.out.println("Programmer error lul");
				return false;
		}
	}
	
	/**
	 * Checks to see if the inputed move is valid for a pawn
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param selectRank the rank 0-7 for the row of the piece being moved
	 * @param selectFile the file 0-7 for the column of the piece being moved
	 * @param destRank the rank 0-7 for the row of the destination
	 * @param destFile the file 0-7 for the column of the destination
	 * @param whiteTurn the boolean value which is true if it is the white player's turn or false if it is the black player's turn 
	 * @return true if the move is valid, false if not
	 */
	
	public static boolean isValidMovePawn (String[][] chessBoard, int selectRank, int selectFile, int destRank, int destFile, boolean whiteTurn) {
		// Check to see f the user is trying to move a piece on top of its own piece
		if (chessBoard[selectRank][selectFile].charAt(0) == chessBoard[destRank][destFile].charAt(0)) {
			return false;
		}	
		
		// Since pawns can only move forward, we need to keep track of turns
		if (whiteTurn) {
			// Check to see if pawn is moving either one or two spaces
			// If it's moving one space, check to see if it's moving straight
			// or "eating" an enemy and check if it's colliding w/ anything
			if ((selectRank - destRank) == 1) {
				// If moving directly forward, check to see if it's an empty space
				if (selectFile == destFile) {
					if (chessBoard[destRank][destFile].charAt(0) == '#' ||
						chessBoard[destRank][destFile].charAt(0) == ' ') {
						return true;
					}
				}
				// Else if moving diagonal, check to see if there's an enemy
				else if ((selectFile - destFile) == 1 || (destFile - selectFile) == 1) {
					if (chessBoard[destRank][destFile].charAt(0) == 'b') {
						return true;
					}
					// Else if check if the prev move was a pawn moving two spaces
					// for en passant
					else if (prevDestRank - prevSelectRank == 2 && chessBoard[prevDestRank][prevDestFile].charAt(1) == 'p'
							&& prevDestFile == destFile) {
						enPassantFlag = true;
						return true;
					}
				}
				return false;
			}
			// If it's moving two spaces, check to see if it's moving straight
			// in starting position or if it's colliding w/ anything
			else if ((selectRank - destRank) == 2 && selectRank == 6 && selectFile == destFile) {
				if ((chessBoard[destRank][destFile].charAt(0) == '#' || chessBoard[destRank][destFile].charAt(0) == ' ') &&
					(chessBoard[destRank+1][destFile].charAt(0) == '#' || chessBoard[destRank+1][destFile].charAt(0) == ' ')){
					return true;
				}
				return false;
			}
			else {
				return false;
			}	
		}
		else {
			// Check to see if pawn is moving either one or two spaces
			// If it's moving one space, check to see if it's moving straight
			// or "eating" an enemy and check if it's colliding w/ anything
			if ((destRank - selectRank) == 1) {
				// If moving directly forward, check to see if it's an empty space
				if (selectFile == destFile) {
					if (chessBoard[destRank][destFile].charAt(0) == '#' ||
						chessBoard[destRank][destFile].charAt(0) == ' ') {
						return true;
					}
				}
				// Else if moving diagonal, check to see if there's an enemy
				else if ((selectFile - destFile) == 1 || (destFile - selectFile) == 1) {
					if (chessBoard[destRank][destFile].charAt(0) == 'w') {
						return true;
					}
					// Else if check if the prev move was a pawn moving two spaces
					// for en passant
					else if (prevSelectRank - prevDestRank == 2 && chessBoard[prevDestRank][prevDestFile].charAt(1) == 'p'
							&& prevDestFile == destFile) {
						enPassantFlag = true;
						return true;
					}
				}
				return false;
			}
			// If it's moving two spaces, check to see if it's moving straight
			// in starting position or if it's colliding w/ anything
			else if ((destRank - selectRank) == 2 && selectRank == 1 && selectFile == destFile) {
				if ((chessBoard[destRank][destFile].charAt(0) == '#' || chessBoard[destRank][destFile].charAt(0) == ' ') &&
					(chessBoard[destRank-1][destFile].charAt(0) == '#' || chessBoard[destRank-1][destFile].charAt(0) == ' ')){
					return true;
				}
				return false;
			}
			else {
				return false;
			}
		}
	}
	
	/**
	 * Checks to see if the inputed move is valid for a rook
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param selectRank the rank 0-7 for the row of the piece being moved
	 * @param selectFile the file 0-7 for the column of the piece being moved
	 * @param destRank the rank 0-7 for the row of the destination
	 * @param destFile the file 0-7 for the column of the destination
	 * @return true if the move is valid, false if not
	 */
	
	public static boolean isValidMoveRook (String[][] chessBoard, int selectRank, int selectFile, int destRank, int destFile) {
		
		// Check to see if the user is trying to move a piece on top of its own piece
		if (chessBoard[selectRank][selectFile].charAt(0) == chessBoard[destRank][destFile].charAt(0)) {
			return false;
		}	
		
		// If the rook is trying to move left and right, check to see if it
		// is colliding w/ anything
		if (selectRank == destRank && selectFile != destFile) {
			if (selectFile > destFile) {
				for (int i = destFile+1; i < selectFile; i++) {
					if (chessBoard[selectRank][i].charAt(0) != '#' && chessBoard[selectRank][i].charAt(0) != ' ') {
						return false;
					}
				}
				return true;
			}
			else {
				for (int i = selectFile+1; i < destFile; i++) {
					if (chessBoard[selectRank][i].charAt(0) != '#' && chessBoard[selectRank][i].charAt(0) != ' ') {
						return false;
					}
				}
				return true;
			}
		}
		// If the rook is trying to move up and down, check to see if it
		// is colliding w/ anything
		else if (selectFile == destFile && selectRank != destRank) {
			if (selectRank > destRank) {
				for (int i = destRank+1; i < selectRank; i++) {
					if (chessBoard[i][selectFile].charAt(0) != '#' && chessBoard[i][selectFile].charAt(0) != ' ') {
						return false;
					}
				}
				return true;
			}
			else {
				for (int i = selectRank+1; i < destRank; i++) {
					if (chessBoard[i][selectFile].charAt(0) != '#' && chessBoard[i][selectFile].charAt(0) != ' ') {
						return false;
					}
				}
				return true;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Checks to see if the inputed move is valid for a knight
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param selectRank the rank 0-7 for the row of the piece being moved
	 * @param selectFile the file 0-7 for the column of the piece being moved
	 * @param destRank the rank 0-7 for the row of the destination
	 * @param destFile the file 0-7 for the column of the destination
	 * @return true if the move is valid, false if not
	 */
	
	public static boolean isValidMoveKnight (String[][] chessBoard, int selectRank, int selectFile, int destRank, int destFile) {		
		// Check to see if the user is trying to move a piece on top of its own piece
		if (chessBoard[selectRank][selectFile].charAt(0) == chessBoard[destRank][destFile].charAt(0)) {
			return false;
		}		
		
		// If the user is moving the knight in an "L" shape
		if (Math.abs(selectRank-destRank) == 2 && Math.abs(selectFile-destFile) == 1 ||
			Math.abs(selectRank-destRank) == 1 && Math.abs(selectFile-destFile) == 2) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Checks to see if the inputed move is valid for a bishop
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param selectRank the rank 0-7 for the row of the piece being moved
	 * @param selectFile the file 0-7 for the column of the piece being moved
	 * @param destRank the rank 0-7 for the row of the destination
	 * @param destFile the file 0-7 for the column of the destination
	 * @return true if the move is valid, false if not
	 */
	
	public static boolean isValidMoveBishop (String[][] chessBoard, int selectRank, int selectFile, int destRank, int destFile) {
		// Check to see if the user is trying to move a piece on top of its own piece
		if (chessBoard[selectRank][selectFile].charAt(0) == chessBoard[destRank][destFile].charAt(0)) {
			return false;
		}		
		
		// If the bishop is moving diagonal, check to see if it's colliding
		// with any pieces
		if (Math.abs(selectRank-destRank) == Math.abs(selectFile-destFile)) {
			// If the bishop is moving upper-left diagonal
			if ((selectRank-destRank) > 0 && (selectFile-destFile) > 0) {
				for (int i = 1; i < (selectRank-destRank); i++) {
					if (chessBoard[destRank+i][destFile+i].charAt(0) != '#' && 
						chessBoard[destRank+i][destFile+i].charAt(0) != ' ') {
						return false;
					}
				}
				return true;
			}	
			// If the bishop is moving upper-right diagonal
			else if ((selectRank-destRank) > 0 && (selectFile-destFile) < 0) {
				for (int i = 1; i < (selectRank-destRank); i++) {
					if (chessBoard[destRank+i][destFile-i].charAt(0) != '#' &&
						chessBoard[destRank+i][destFile-i].charAt(0) != ' ') {
						return false;
					}
				}
				return true;
			}
			// If the bishop is moving down-right diagonal
			else if ((selectRank-destRank) < 0 && (selectFile-destFile) > 0) {
				for (int i = 1; i < (destRank-selectRank); i++) {
					if (chessBoard[destRank-i][destFile+i].charAt(0) != '#' &&
						chessBoard[destRank-i][destFile+i].charAt(0) != ' ') {
						return false;
					}
				}
				return true;
			}
			// If the bishop is moving down-left diagonal
			else if ((selectRank-destRank) < 0 && (selectFile-destFile) < 0) {
				for (int i = 1; i < (destRank-selectRank); i++) {
					if (chessBoard[destRank-i][destFile-i].charAt(0) != '#' &&
						chessBoard[destRank-i][destFile-i].charAt(0) != ' ') {
						return false;
					}
				}
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Checks to see if the inputed move is valid for a queen
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param selectRank the rank 0-7 for the row of the piece being moved
	 * @param selectFile the file 0-7 for the column of the piece being moved
	 * @param destRank the rank 0-7 for the row of the destination
	 * @param destFile the file 0-7 for the column of the destination
	 * @return true if the move is valid, false if not
	 */
	
	public static boolean isValidMoveQueen (String[][] chessBoard, int selectRank, int selectFile, int destRank, int destFile) {
		// Check to see if the user is trying to move a piece on top of its own piece
		if (chessBoard[selectRank][selectFile].charAt(0) == chessBoard[destRank][destFile].charAt(0)) {
			return false;
		}		
		
		// Since a queen moves like a rook or bishop, check to see if it satisfies
		// the moves of a rook or bishop
		if (isValidMoveRook(chessBoard, selectRank, selectFile, destRank, destFile) || 
			isValidMoveBishop(chessBoard, selectRank, selectFile, destRank, destFile)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Checks to see if the inputed move is valid for a king
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param selectRank the rank 0-7 for the row of the piece being moved
	 * @param selectFile the file 0-7 for the column of the piece being moved
	 * @param destRank the rank 0-7 for the row of the destination
	 * @param destFile the file 0-7 for the column of the destination
	 * @return true if the move is valid, false if not
	 */
	
 	public static boolean isValidMoveKing (String[][] chessBoard, int selectRank, int selectFile, int destRank, int destFile) {
		// Check to see if the user is trying to move a piece on top of its own piece
		if (chessBoard[selectRank][selectFile].charAt(0) == chessBoard[destRank][destFile].charAt(0)) {
			return false;
		}
 		
 		// Since a king is allowed to move a space of 1, check to see if it moves
		// 1 space away from it's position
		if (Math.abs(selectRank-destRank) <= 1 && Math.abs(selectFile-destFile) <= 1 && 
				((selectRank-destRank) != 0 || (selectFile-destFile) != 0)) {
			return true;
		}
		else if (Math.abs(selectFile-destFile) == 2 && selectRank == destRank) {
			// Check to make sure the King isn't capturing
			if (chessBoard[destRank][destFile].charAt(0) == '#' || chessBoard[destRank][destFile].charAt(0) == ' ') {
				// Check to see if whiteTurn
				if (chessBoard[selectRank][selectFile].charAt(0) == 'w') {
					if (selectFile > destFile) {
						if (whiteCastleKing && whiteCastleRook1) {
							if (chessBoard[7][1].equals("  ") && chessBoard[7][2].equals("##") && chessBoard[7][3].equals("  ")) {
								castleFlag = true;
								return true;
							}
						}

					}
					else {
						if (whiteCastleKing && whiteCastleRook2) {
							if (chessBoard[7][5].equals("  ") && chessBoard[7][6].equals("##")) {
								castleFlag = true;
								return true;
							}
						}	
					}
				}
				else {
					if (selectFile > destFile) {
						if (blackCastleKing && blackCastleRook1) {
							if (chessBoard[0][1].equals("##") && chessBoard[0][2].equals("  ") && chessBoard[0][3].equals("##")) {
								castleFlag = true;
								return true;
							}
						}

					}
					else {
						if (blackCastleKing && blackCastleRook2) {
							if (chessBoard[0][5].equals("##") && chessBoard[0][6].equals("  ")) {
								castleFlag = true;
								return true;
							}
						}				
					}
				}
			}
		}
		else {
			return false;
		}
		return false;
	}
	
	/**
	 * Takes the users input and either moves the selected piece, does a special move (en passant / castle), or ends the game on resign or draw
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param input the players input
	 * @param whiteTurn the boolean value which is true if it is the white player's turn or false if it is the black player's turn 
	 */
 	
	public static void processMove (String [][] chessBoard, String input, boolean whiteTurn) {
		// Check to see if move is resign/draw
		if (input.toLowerCase().equals("resign")) {
			if (whiteTurn) {
				System.out.println("Black wins");
				System.exit(0);
			}
			else {
				System.out.println("White wins");
				System.exit(0);
			}
		}
		else if (input.toLowerCase().equals("draw")) {
			System.exit(0);
		}
		
		// Parse input into selectFile, selectRank, destFile, destRank
		int selectFile, selectRank, destFile, destRank;
		
		// Use switch-case statements to turn raw values into values that
		// the program can use
		switch(Character.toLowerCase(input.charAt(0))) {
			case 'a': 
				selectFile = 0;
				break;
			case 'b':
				selectFile = 1;
				break;
			case 'c':
				selectFile = 2;
				break;
			case 'd':
				selectFile = 3;
				break;
			case 'e':
				selectFile = 4;
				break;
			case 'f':
				selectFile = 5;
				break;
			case 'g':
				selectFile = 6;
				break;
			case 'h':
				selectFile = 7;
				break;
			default:
				System.out.println("Programmer error lul");
				return;
		}
		switch(Integer.parseInt(input.substring(1,2))) {
			case 8:
				selectRank = 0;
				break;
			case 7:
				selectRank = 1;
				break;
			case 6:
				selectRank = 2;
				break;
			case 5: 
				selectRank = 3;
				break;
			case 4:
				selectRank = 4;
				break;
			case 3:
				selectRank = 5;
				break;
			case 2:
				selectRank = 6;
				break;
			case 1:
				selectRank = 7;
				break;
			default:
				System.out.println("Programmer error lul");
				return;
		}	
		switch(Character.toLowerCase(input.charAt(3))) {
			case 'a': 
				destFile = 0;
				break;
			case 'b':
				destFile = 1;
				break;
			case 'c':
				destFile = 2;
				break;
			case 'd':
				destFile = 3;
				break;
			case 'e':
				destFile = 4;
				break;
			case 'f':
				destFile = 5;
				break;
			case 'g':
				destFile = 6;
				break;
			case 'h':
				destFile = 7;
				break;
			default:
				System.out.println("Programmer error lul");
				return;
		}		
		switch(Integer.parseInt(input.substring(4,5))) {
			case 8:
				destRank = 0;
				break;
			case 7:
				destRank = 1;
				break;
			case 6:
				destRank = 2;
				break;
			case 5: 
				destRank = 3;
				break;
			case 4:
				destRank = 4;
				break;
			case 3:
				destRank = 5;
				break;
			case 2:
				destRank = 6;
				break;
			case 1:
				destRank = 7;
				break;
			default:
				System.out.println("Programmer error lul");
				return;
		}
		
		// If enPassant flag is thrown, "eat" the pawn behind the pawn
		// that was moved
		if (enPassantFlag) {
			if (whiteTurn) {
				if ((destRank+1 % 2 == 0) && (destFile % 2 == 0)) {
					chessBoard[destRank+1][destFile] = "  ";
				}
				else if ((destRank+1 % 2 != 0) && (destFile % 2 == 0)) {
					chessBoard[destRank+1][destFile] = "##";
				}
				else if ((destRank+1 % 2 == 0) && (destFile % 2 != 0)) {
					chessBoard[destRank+1][destFile] = "##";
				}
				else {
					chessBoard[destRank+1][destFile] = "  ";
				}
			}
			else {
				if (((destRank-1) % 2 == 0) && (destFile % 2 == 0)) {
					chessBoard[destRank-1][destFile] = "  ";
				}
				else if (((destRank-1) % 2 != 0) && (destFile % 2 == 0)) {
					chessBoard[destRank-1][destFile] = "##";
				}
				else if (((destRank-1) % 2 == 0) && (destFile % 2 != 0)) {
					chessBoard[destRank-1][destFile] = "##";
				}
				else {
					chessBoard[destRank-1][destFile] = "  ";
				}				
			}
			// Reset flag 
			enPassantFlag = false;
		}
		else if (castleFlag) {
			if (whiteTurn) {
				if (selectFile > destFile) {
					chessBoard[7][3] = "wR";
					chessBoard[7][0] = "##";
				}
				else {
					chessBoard[7][5] = "wR";
					chessBoard[7][7] = "  ";
				}
			}
			else {
				if (selectFile > destFile) {
					chessBoard[0][3] = "bR";
					chessBoard[0][0] = "  ";
				}
				else {
					chessBoard[0][5] = "bR";
					chessBoard[0][7] = "##";
				}
			}
			// Reset flag
			castleFlag = false;
		}
		
		// If pawn, check to see if it is up for promotion
		if (chessBoard[selectRank][selectFile].charAt(1) == 'p') { 
			if (whiteTurn) {
				if (destRank == 0) {
					// Check to see if a promotion piece was specified
					if (input.length() == 7 && (input.charAt(6) == 'R' || input.charAt(6) == 'N' ||
						input.charAt(6) == 'B' || input.charAt(6) == 'Q')) {
						chessBoard[selectRank][selectFile] = "w".concat(Character.toString(input.charAt(6)));
					}
					else {
						chessBoard[selectRank][selectFile] = "wQ";
					}
				}
			}
			else {
				if (destRank == 7) {
					// Check to see if a promotion piece was specified
					if (input.length() == 7 && (input.charAt(6) == 'R' || input.charAt(6) == 'N' ||
						input.charAt(6) == 'B' || input.charAt(6) == 'Q')) {
						chessBoard[selectRank][selectFile] = "b".concat(Character.toString(input.charAt(6)));
					}
					else {
						chessBoard[selectRank][selectFile] = "bQ";
					}
				}
			}
		}
		
		// Set off flags if kings or rooks are moved
		if (chessBoard[selectRank][selectFile].charAt(1) == 'K') {
			if (whiteTurn) {
				whiteCastleKing = false;
			}
			else {
				blackCastleKing = false;
			}
		}
		else if (chessBoard[selectRank][selectFile].charAt(1) == 'R') {
			if (whiteTurn) {
				if (selectFile == 0) {
					whiteCastleRook1 = false;
				}
				else {
					whiteCastleRook2 = false;
				}
			}
			else {
				if (selectFile == 0) {
					blackCastleRook1 = false;
				}
				else {
					blackCastleRook2 = false;
				}
			}
		}
		
		
		// Move the piece to the desired location
		chessBoard[destRank][destFile] = chessBoard[selectRank][selectFile];
		
		if ((selectRank % 2 == 0) && (selectFile % 2 == 0)) {
			chessBoard[selectRank][selectFile] = "  ";
		}
		else if ((selectRank % 2 != 0) && (selectFile % 2 == 0)) {
			chessBoard[selectRank][selectFile] = "##";
		}
		else if ((selectRank % 2 == 0) && (selectFile % 2 != 0)) {
			chessBoard[selectRank][selectFile] = "##";
		}
		else {
			chessBoard[selectRank][selectFile] = "  ";
		}
		
		// Keep track of previous inputs
		prevSelectFile = selectFile;
		prevSelectRank = selectRank;
		prevDestFile = destFile;
		prevDestRank = destRank;
		
		// If inCheck was flagged, and passed the validMove method, then reset the flag
		inCheck = false;
		
		return;
	}
	
	/**
	 * isCheck reviews each of the player's pieces to see if they can take the opponent's king
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param whiteTurn the boolean value which is true if it is the white player's turn or false if it is the black player's turn 
	 * @return true if the player has the opponent in check, false if not
	 */
	
	public static boolean isCheck (String[][] chessBoard, boolean whiteTurn) {
		int kingFile = -1; 
		int kingRank = -1;
		if (whiteTurn) {
			// Find the opposing King's position on the board
			for (int i = 0; i < chessBoard.length; i++) {
				for (int j = 0; j < chessBoard[i].length; j++) {
					if (chessBoard[i][j].equals("bK")) {
						kingRank = i;
						kingFile = j;
					}
				}
			}
			// Check each of white's pieces
			for (int i = 0; i < chessBoard.length; i++) {
				for (int j = 0; j < chessBoard[i].length; j++) {
					if (chessBoard[i][j].charAt(0) == 'w') {
						switch (chessBoard[i][j].charAt(1)) {
							case 'p':
								if(isValidMovePawn(chessBoard, i, j, kingRank, kingFile, whiteTurn)) {
									return true;
								}
								break;
							case 'R':
								if(isValidMoveRook(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							case 'N':
								if(isValidMoveKnight(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							case 'B':
								if(isValidMoveBishop(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							case 'Q':
								if(isValidMoveQueen(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							case 'K':
								if(isValidMoveKing(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							default:
								break;
						}
					}
				}
			}
		}
		else {
			// Find the opposing King's position on the board
			for (int i = 0; i < chessBoard.length; i++) {
				for (int j = 0; j < chessBoard[i].length; j++) {
					if (chessBoard[i][j].equals("wK")) {
						kingRank = i;
						kingFile = j;
					}
				}
			}
			// Check each of black's pieces
			for (int i = 0; i < chessBoard.length; i++) {
				for (int j = 0; j < chessBoard[i].length; j++) {
					if (chessBoard[i][j].charAt(0) == 'b') {
						switch (chessBoard[i][j].charAt(1)) {
							case 'p':
								if(isValidMovePawn(chessBoard, i, j, kingRank, kingFile, whiteTurn)) {
									return true;
								}
								break;
							case 'R':
								if(isValidMoveRook(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							case 'N':
								if(isValidMoveKnight(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							case 'B':
								if(isValidMoveBishop(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							case 'Q':
								if(isValidMoveQueen(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							case 'K':
								if(isValidMoveKing(chessBoard, i, j, kingRank, kingFile)) {
									return true;
								}
								break;
							default:
								break;
						}
					}
				}
			}			
		}
		return false;
	}
	
	/**
	 * Checks to see if player has opponent in CheckMate
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param whiteTurn the boolean value which is true if it is the white player's turn or false if it is the black player's turn 
	 * @return true if player has opponent in CheckMate, false if not
	 */
	
	public static boolean isCheckmate (String[][] chessBoard, boolean whiteTurn) {
		int kingFile = -1; 
		int kingRank = -1;
		if (whiteTurn) {
			// Find the opposing King's position on the board
			for (int i = 0; i < chessBoard.length; i++) {
				for (int j = 0; j < chessBoard[i].length; j++) {
					if (chessBoard[i][j].equals("bK")) {
						kingRank = i;
						kingFile = j;
					}
				}
			}
			// Check to see any valid moves for king
			if (canKingMove(chessBoard, kingRank, kingFile, whiteTurn)) {
				return false;
			}
			
			// Check to see if any valid moves for other pieces
			if (canPiecesMove(chessBoard, whiteTurn)) {
				return false;
			}

			// If the opponent cannot make any valid moves, then it's checkmate
			return true;			
		}
		else {
			// Find the opposing King's position on the board
			for (int i = 0; i < chessBoard.length; i++) {
				for (int j = 0; j < chessBoard[i].length; j++) {
					if (chessBoard[i][j].equals("wK")) {
						kingRank = i;
						kingFile = j;
						break;
					}
				}
			}
			// Check to see any valid moves for king
			if (canKingMove(chessBoard, kingRank, kingFile, whiteTurn)) {
				return false;
			}
			
			// Check to see if any valid moves for other pieces
			if (canPiecesMove(chessBoard, whiteTurn)) {
				return false;
			}

			// If the opponent cannot make any valid moves, then it's checkmate
			return true;
		}
	}
	
	/**
	 * Checks to see if the king has any valid moves it can make that would take it out of the check position. 
	 * This method uses a copy of the chessBoard to move around the king and check to see if any of the 8 spaces
	 * would be a valid move for next turn.
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param kingRank the rank 0-7 for the row of the King being moved
	 * @param kingFile the file 0-7 for the column of the piece being moved
	 * @param whiteTurn the boolean value which is true if it is the white player's turn or false if it is the black player's turn
	 * @return a boolean value of true if the king can move to any space w/o being in check or false if the king can't move to a place
	 */
	
	public static boolean canKingMove (String[][] chessBoard, int kingRank, int kingFile, boolean whiteTurn) {
		String[][] proxyChessBoard = new String[chessBoard.length][chessBoard.length];
		
		// Check all adjacent spots to see if king can move anywhere
		// If king can move in any adjacent spot, return true
		if (((kingRank+1) >= 0 && (kingRank+1) < 8)) {			
			if (isValidMoveKing(chessBoard, kingRank, kingFile, kingRank+1, kingFile)) {	
				copyBoard(proxyChessBoard, chessBoard);
				
				// Move the king on the proxyChessBoard
				if (whiteTurn) {
					proxyChessBoard[kingRank+1][kingFile] = "bK";
				}
				else {
					proxyChessBoard[kingRank+1][kingFile] = "wK";
				}
				proxyChessBoard[kingRank][kingFile] = "  ";
				
				if (!isCheck(proxyChessBoard, whiteTurn)) {
					return true;
				}
			}
		}
		if (((kingRank-1) >= 0 && (kingRank-1) < 8)) {			
			if (isValidMoveKing(chessBoard, kingRank, kingFile, kingRank-1, kingFile)) {
				copyBoard(proxyChessBoard, chessBoard);
	
				// Move the king on the proxyChessBoard
				if (whiteTurn) {
					proxyChessBoard[kingRank-1][kingFile] = "bK";
				}
				else {
					proxyChessBoard[kingRank-1][kingFile] = "wK";
				}
				proxyChessBoard[kingRank][kingFile] = "  ";
				
				if (!isCheck(proxyChessBoard, whiteTurn)) {
					return true;
				}
			}
		}
		if (((kingFile+1) >= 0 && (kingFile+1) < 8)) {
			if (isValidMoveKing(chessBoard, kingRank, kingFile, kingRank, kingFile+1)) {
				copyBoard(proxyChessBoard, chessBoard);
				
				// Move the king on the proxyChessBoard
				if (whiteTurn) {
					proxyChessBoard[kingRank][kingFile+1] = "bK";
				}
				else {
					proxyChessBoard[kingRank][kingFile+1] = "wK";
				}
				proxyChessBoard[kingRank][kingFile] = "  ";
				
				if (!isCheck(proxyChessBoard, whiteTurn)) {
					return true;
				}
			}
		}
		if (((kingFile-1) >= 0 && (kingFile-1) < 8)) {
			if(isValidMoveKing(chessBoard, kingRank, kingFile, kingRank, kingFile-1)) {
				copyBoard(proxyChessBoard, chessBoard);
				
				// Move the king on the proxyChessBoard
				if (whiteTurn) {
					proxyChessBoard[kingRank][kingFile-1] = "bK";
				}
				else {
					proxyChessBoard[kingRank][kingFile-1] = "wK";
				}
				proxyChessBoard[kingRank][kingFile] = "  ";
				
				if (!isCheck(proxyChessBoard, whiteTurn)) {
					return true;
				}
			}
		}
		if (((kingFile-1) >= 0 && (kingFile-1) < 8) & ((kingRank+1) >= 0 && (kingRank+1) < 8)) {
			if(isValidMoveKing(chessBoard, kingRank, kingFile, kingRank+1, kingFile-1)) {
				copyBoard(proxyChessBoard, chessBoard);
				
				// Move the king on the proxyChessBoard
				if (whiteTurn) {
					proxyChessBoard[kingRank+1][kingFile-1] = "bK";
				}
				else {
					proxyChessBoard[kingRank+1][kingFile-1] = "wK";
				}
				proxyChessBoard[kingRank][kingFile] = "  ";
				
				if (!isCheck(proxyChessBoard, whiteTurn)) {
					return true;
				}
			}
		}
		if (((kingFile-1) >= 0 && (kingFile-1) < 8) & ((kingRank-1) >= 0 && (kingRank-1) < 8)) {
			if (isValidMoveKing(chessBoard, kingRank, kingFile, kingRank-1, kingFile-1)) {
					copyBoard(proxyChessBoard, chessBoard);
					
					// Move the king on the proxyChessBoard
					if (whiteTurn) {
						proxyChessBoard[kingRank-1][kingFile-1] = "bK";
					}
					else {
						proxyChessBoard[kingRank-1][kingFile-1] = "wK";
					}
					proxyChessBoard[kingRank][kingFile] = "  ";
					
					if (!isCheck(proxyChessBoard, whiteTurn)) {
						return true;
					}
			}
		}
		if (((kingFile+1) >= 0 && (kingFile+1) < 8) & ((kingRank-1) >= 0 && (kingRank-1) < 8)) {
			if (isValidMoveKing(chessBoard, kingRank, kingFile, kingRank-1, kingFile+1)) {
					copyBoard(proxyChessBoard, chessBoard);
					
					// Move the king on the proxyChessBoard
					if (whiteTurn) {
						proxyChessBoard[kingRank-1][kingFile+1] = "bK";
					}
					else {
						proxyChessBoard[kingRank-1][kingFile+1] = "wK";
					}
					proxyChessBoard[kingRank][kingFile] = "  ";
					
					if (!isCheck(proxyChessBoard, whiteTurn)) {
						return true;
					}
			}
		}
		if (((kingFile+1) >= 0 && (kingFile+1) < 8) & ((kingRank+1) >= 0 && (kingRank+1) < 8)) {
			if (isValidMoveKing(chessBoard, kingRank, kingFile, kingRank+1, kingFile+1)) {
					copyBoard(proxyChessBoard, chessBoard);
					
					// Move the king on the proxyChessBoard
					if (whiteTurn) {
						proxyChessBoard[kingRank+1][kingFile+1] = "bK";
					}
					else {
						proxyChessBoard[kingRank+1][kingFile+1] = "wK";
					}
					proxyChessBoard[kingRank][kingFile] = "  ";
					
					if (!isCheck(proxyChessBoard, whiteTurn)) {
						return true;
					}
			}
		}			
		return false;
	}
	
	/**
	 * Checks to see if any of the pieces can move to a position returned by findValidMoveSpaces to take the king out of the check position.
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param whiteTurn the boolean value which is true if it is the white player's turn or false if it is the black player's turn
	 * @return Returns true if the piece can move and false if it cannot
	 */
	
	public static boolean canPiecesMove (String[][] chessBoard, boolean whiteTurn) {
		ArrayList<int[]> listOfBoardPositions = findValidMoveSpaces (chessBoard, whiteTurn);
		
		// Since the method findValidMoveSpaces returns an ArrayList of board positions
		// that a piece can move to in order to avoid check, we need to check to see if
		// the opponent can move any pieces to any of these board positions
		if (whiteTurn) {
			for (int[] boardPosition : listOfBoardPositions) {
				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						if (chessBoard[i][j].charAt(0) == 'b') {
							switch (chessBoard[i][j].charAt(1)) {
								case 'p':
									if(isValidMovePawn(chessBoard, i, j, boardPosition[0], boardPosition[1], !whiteTurn)) {
										return true;
									}
									break;
								case 'R':
									if(isValidMoveRook(chessBoard, i, j, boardPosition[0], boardPosition[1])) {
										return true;
									}
									break;
								case 'N':
									if(isValidMoveKnight(chessBoard, i, j, boardPosition[0], boardPosition[1])) {
										return true;
									}
									break;
								case 'B':
									if(isValidMoveBishop(chessBoard, i, j, boardPosition[0], boardPosition[1])) {
										return true;
									}
									break;
								case 'Q':
									if(isValidMoveQueen(chessBoard, i, j, boardPosition[0], boardPosition[1])) {
										return true;
									}
									break;
								default:
									break;
							}
						}
					}
				}
			}
		}
		else {
			for (int[] boardPosition : listOfBoardPositions) {
				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						if (chessBoard[i][j].charAt(0) == 'w') {
							switch (chessBoard[i][j].charAt(1)) {
								case 'p':
									if(isValidMovePawn(chessBoard, i, j, boardPosition[0], boardPosition[1], !whiteTurn)) {
										return true;
									}
									break;
								case 'R':
									if(isValidMoveRook(chessBoard, i, j, boardPosition[0], boardPosition[1])) {
										return true;
									}
									break;
								case 'N':
									if(isValidMoveKnight(chessBoard, i, j, boardPosition[0], boardPosition[1])) {
										return true;
									}
									break;
								case 'B':
									if(isValidMoveBishop(chessBoard, i, j, boardPosition[0], boardPosition[1])) {
										return true;
									}
									break;
								case 'Q':
									if(isValidMoveQueen(chessBoard, i, j, boardPosition[0], boardPosition[1])) {
										return true;
									}
									break;
								default:
									break;
							}
						}
					}
				}
			}
		}	
		
		// If none of these pieces can move to the valid spots, then return false
		// This means that whichever pieces the opponent can move won't take their
		// King out of check
		return false;
	}
	
	/**
	 * Finds valid move spaces that a piece can move to in order to not be in check
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @param whiteTurn the boolean value which is true if it is the white player's turn or false if it is the black player's turn
	 * @return an ArrayList of coordinates on the board that are valid places to move to in order to avoid check
	 */
	
	public static ArrayList<int[]> findValidMoveSpaces (String[][] chessBoard, boolean whiteTurn) {
		
		// Instantiate ArrayList of board positions a piece can move in 
		ArrayList<int[]> listOfBoardPositions = new ArrayList<int[]>();		
	
		// Create a proxy chess board to test on
		String[][] proxyChessBoard = new String[chessBoard.length][chessBoard.length];
		copyBoard(proxyChessBoard, chessBoard);
		
		// Use the proxy chess board to place a dummy piece in every space
		// If the isCheck method returns false, then a piece can move to that spot
		// To prevent the King from being 'captured'
		if (whiteTurn) {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					// Don't check to see if pawns can go on a king tile
					if (proxyChessBoard[i][j].equals("bK")) {
						continue;
					}
					String temp = proxyChessBoard[i][j];
					proxyChessBoard[i][j] = "bp";
					if (!isCheck(proxyChessBoard, whiteTurn)) {
						int[] boardPosition = new int[2];
						boardPosition[0] = i;
						boardPosition[1] = j;	
						listOfBoardPositions.add(boardPosition);
					}
					proxyChessBoard[i][j] = temp;
				}
			}
		}
		else {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (proxyChessBoard[i][j].equals("wK")) {
						continue;
					}
					String temp = proxyChessBoard[i][j];
					proxyChessBoard[i][j] = "wp";
					if (!isCheck(proxyChessBoard, whiteTurn)) {
						int[] boardPosition = new int[2];
						boardPosition[0] = i;
						boardPosition[1] = j;
						listOfBoardPositions.add(boardPosition);
					}
					proxyChessBoard[i][j] = temp;
				}
			}
		}
		
		return listOfBoardPositions;
	}
	
	/**
	 * Copies over the contents from chessBoard to proxyChessBoard
	 * @param proxyChessBoard  the 2D string array representation of the chessBoard that's used for testing checkmate scenarios
	 * @param chessBoard  the 2D string array representation of the chessBoard
	 */
	
	public static void copyBoard (String[][] proxyChessBoard, String[][] chessBoard) {
		for (int i = 0; i < chessBoard.length; i++) {
			for (int j = 0; j < chessBoard.length; j++) {
				proxyChessBoard[i][j] = chessBoard[i][j];
			}
		}
	}
	
	/**
	 * Checks the board after each turn to see if there is a stale mate
	 * @param chessBoard the 2D string array representation of the chessBoard
	 * @return true if there are no possible moves and false if there is at least one move possible
	 */
	/*public static boolean isStalemate(String[][] chessBoard) {
		for(int i =0;i<8;i++) {
			for(int j=0; j<8;j++) {
				if(chessBoard[i][j].charAt(1)=='p') {
					for(int k=0;k<8;k++){
						for(int l=0;l<8;l++) {
							if(isValidMovePawn(chessBoard,i,j,k,l,true)) {
								return false;
							}
							if(isValidMovePawn(chessBoard,i,j,k,l,false)) {
								return false;
							}
						}
					}
				}else if(chessBoard[i][j].charAt(1)=='R') {
					for(int k=0;k<8;k++){
						for(int l=0;l<8;l++) {
							if(isValidMoveRook(chessBoard,i,j,k,l)) {
								return false;
							}
						}
					}
				}else if(chessBoard[i][j].charAt(1)=='N') {
					for(int k=0;k<8;k++){
						for(int l=0;l<8;l++) {
							if(isValidMoveKnight(chessBoard,i,j,k,l)) {
								return false;
							}
						}
					}
				}else if(chessBoard[i][j].charAt(1)=='K') {
					for(int k=0;k<8;k++){
						for(int l=0;l<8;l++) {
							if(isValidMoveKing(chessBoard,i,j,k,l)) {
								return false;
							}
						}
					}
				}else if(chessBoard[i][j].charAt(1)=='Q') {
					for(int k=0;k<8;k++){
						for(int l=0;l<8;l++) {
							if(isValidMoveQueen(chessBoard,i,j,k,l)) {
								return false;
							}
						}
					}
				}
			}
		}
		
		return true;
	}*/
	
	/**
	 * Prints the string representation of the current state of the chessBoard
	 * @param chessBoard the 2D string array representation of the chessBoard
	 */

	public static void printChessBoard (String[][] chessBoard) {
		System.out.println();
		for (int i = 0; i < chessBoard.length; i++) {
			for (int j = 0; j < chessBoard.length; j++) {
				System.out.print(chessBoard[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
